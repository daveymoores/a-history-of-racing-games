# History of Racing Games
 #

Using...

* Foundation
* ScotchBox
* Scrollmagic
* Greensock
* Velocity

Scotch Box
==========

## Check out the official docs at: [box.scotch.io][16]
## [Read the getting started article](https://scotch.io/bar-talk/introducing-scotch-box-a-vagrant-lamp-stack-that-just-works)
## [Read the 2.0 release article](https://scotch.io/bar-talk/announcing-scotch-box-2-0-our-dead-simple-vagrant-lamp-stack-improved)