$(document).foundation();



$(function(){


   var ua = navigator.userAgent;
    function is_touch_device() {
        try {
            document.createEvent("TouchEvent");
            return true;
        } catch (e) {
            return false;
        }
    }

    if ((is_touch_device()) || ua.match(/(iPhone|iPod|iPad)/)
    || ua.match(/BlackBerry/) || ua.match(/Android/)) {

      $('#videoGamesWrapper').find('.lazy').each(function(){
          $(this).attr('src',$(this).data('original'));
      });

      console.log('touch');

      $('#scrollwrapper').css('overflow', 'scroll');
      $('#dragmodal').find('p').text('Drag to explore');


      if ($( window ).width() <= 600) {
         $('.gamecarousel__container').slick({
            arrows : false,
            dots: true,
            centerMode: true,
            centerPadding: '40px',
            slidesToShow: 1
         });

         $('#alldates').remove();

      }

      if ($( window ).width() >= 600) {

         $('#videoGamesWrapper').find('.gamecarousel').remove();
         $('#videoGamesWrapper').find('.date__wrapper').remove();
         $('#videoGamesWrapper').find('.aim').remove();
         $('#videoGamesWrapper').find('.test').remove();

      }

    } else {


      console.log('NO touch');

      //   $('#scrollwrapper').kinetic({
      //        maxvelocity : 20,
      //        throttleFPS : 100,
      //        slowdown : 0.93
      //   });

      // $(window).on('IGNScroll.staticUpExample', {
      //     threshold: 200
      // });

      // var myScroll = new IScroll('#scrollwrapper', { mouseWheel: true, bounceEasing: 'elastic', bounceTime: 1200 });

        $('.gamecarousel').remove();
        $('#videoGamesWrapper').find('.date__wrapper').remove();
        $('#videoGamesWrapper').find('.aim').remove();
        $('#videoGamesWrapper').find('.test').remove();

        //----image lazy load--//
        $("img.lazy").lazyload({
             effect : "fadeIn",
             failure_limit : 10
        });
        //--end lazy load ---//


    }








   $('.open').on('click', function(){

      window.scrollTo(0, 0);
      $('.off-canvas-wrap').foundation('offcanvas', 'show', 'move-right');

   });






   //
   // $('#scrollwrapper').kinetic({
   //     'moved': function(){ $('p').addClass('inselectable'); },
   //     'stopped': function(){ $('p').removeClass('inselectable'); }
   // });
   // // Sometimes stopped doesn't get called last
   // $('body').mouseup(function(){$('p').removeClass('inselectable');});




   console.log($('#videoGamesWrapper').find('.videogames__container').length);



      var startpath = {
			stageexit : {
				curviness: 0,
				autoRotate: false,
				values: [
						{x: 0,	y: 0},
						{x: 0,	y: 500}
					]
			}
		};

      var k=1700;



      //-------- left to right flight path
		var flightpath = {
			entry : {
				curviness: 1.25,
				autoRotate:["x","y","rotation",270,false],
				values: [
						{x: 0,	y: -(720 + k)},
						{x: 100,	y: -(200 + k)}
					]
			},
			leave : {
				curviness: 1.25,
				autoRotate:["x","y","rotation",270,false],
				values: [
                  {x: 250,	y: -(880 + k)},
						{x: 660,	y: -(780 + k)},
						{x: 800,	y: -(1030 + k)},
                  {x: 900,	y: -(930 + k)},
                  {x: 1100,	y: -(900 + k)},
						{x: $(window).width() + 700,	y: -(1600 + k)}
					]
			}
		};

      var flightpath2 = {
         leave : {
            curviness: 1.25,
            autoRotate:["x","y","rotation",270,false],
            values: [
               {x: 250,	y: -(880 + k)},
               {x: 660,	y: -(780 + k)},
               {x: 800,	y: -(1030 + k)},
               {x: 900,	y: -(930 + k)},
               {x: 1100,	y: -(900 + k)},
               {x: $(window).width() + 700,	y: -(1600 + k)}
            ]
         }
      };


      //-------- right to left flight path

      var q = 0;

      var rtlflightpath = {
         entry : {
            curviness: 1.5,
            autoRotate:["x","y","rotation",270,false],
            values: [
                  {x: -100,	y: 200},
                  {x: -400,	y: -500},
                  {x: -800,	y: -1000},
                  {x: -1000,	y: -900},
                  {x: -1300,	y: -1200},
                  {x: -1600,	y: -1800},
                  {x: -1900,	y: -1700},
                  {x: -2100,	y: -1600}
               ]
         }
      };




		// init controller
		var controller = new ScrollMagic.Controller();
      var dateController = new ScrollMagic.Controller();
      var parallaxcontroller = new ScrollMagic.Controller({globalSceneOptions: {triggerHook: "onEnter", duration: "200%"}});



      //-------remove controllers based on screen size

      if ((is_touch_device()) || ua.match(/(iPhone|iPod|iPad)/)
      || ua.match(/BlackBerry/) || ua.match(/Android/)) {
         controller.enabled(false);
         dateController.enabled(false);
         parallaxcontroller.enabled(false);
      }




		//------------ left to right cars
		var car1 = new TimelineMax()
			.add(TweenMax.to($("#car_1"), 1.2, {css:{bezier:flightpath.entry}, ease:Power1.easeInOut}))
			.add(TweenMax.to($("#car_1"), 1, {css:{bezier:flightpath.leave}, ease:Power1.easeInOut}));

      var car2 = new TimelineMax()
         .add(TweenMax.to($("#car_2"), 1, {css:{bezier:flightpath.entry}, ease:Power1.easeInOut}))
         .add(TweenMax.to($("#car_2"), 1.1, {css:{bezier:flightpath.leave}, ease:Power1.easeInOut}));

      var car3 = new TimelineMax()
         .add(TweenMax.to($("#car_3"), 0.7, {css:{bezier:flightpath.entry}, ease:Power1.easeInOut}))
         .add(TweenMax.to($("#car_3"), 1, {css:{bezier:flightpath2.leave}, ease:Power1.easeInOut}));


      // create tween
		var carrtl1 = new TimelineMax()
			.add(TweenMax.to($("#car-rtl_1"), 1, {css:{bezier:rtlflightpath.entry}, ease:Power1.easeInOut}));
			//.add(TweenMax.to($("#car-rtl_1"), 1, {css:{bezier:rtlflightpath.leave}, ease:Power1.easeInOut}));

      var carrtl2 = new TimelineMax()
         .add(TweenMax.to($("#car-rtl_2"), 2, {css:{bezier:rtlflightpath.entry}, ease:Power1.easeInOut}));
         //.add(TweenMax.to($("#car-rtl_2"), 1.1, {css:{bezier:rtlflightpath.leave}, ease:Power1.easeInOut}));

      var carrtl3 = new TimelineMax()
         .add(TweenMax.to($("#car-rtl_3"), 0.1, {css:{bezier:rtlflightpath.entry}, ease:Power1.easeInOut}));
         //.add(TweenMax.to($("#car-rtl_3"), 1, {css:{bezier:rtlflightpath2.leave}, ease:Power1.easeInOut}));




      //end tween
      var car12 = new TimelineMax()
			.add(TweenMax.to($("#car_4"), 1, {css:{bezier:startpath.stageexit}, ease:Power1.easeIn}));

      var car22 = new TimelineMax()
         .add(TweenMax.to($("#car_5"), 1, {css:{bezier:startpath.stageexit}, ease:Power1.easeIn}));

      var car32 = new TimelineMax()
         .add(TweenMax.to($("#car_6"), 1, {css:{bezier:startpath.stageexit}, ease:Power1.easeIn}));

      var car42 = new TimelineMax()
			.add(TweenMax.to($("#car_7"), 1, {css:{bezier:startpath.stageexit}, ease:Power1.easeIn}));

      var car52 = new TimelineMax()
         .add(TweenMax.to($("#car_8"), 1, {css:{bezier:startpath.stageexit}, ease:Power1.easeIn}));


      //end tween
      var carfinish12 = new TimelineMax()
			.add(TweenMax.to($("#carfinish_4"), 1, {css:{bezier:startpath.stageexit}, ease:Power1.easeIn}));

      var carfinish22 = new TimelineMax()
         .add(TweenMax.to($("#carfinish_5"), 1, {css:{bezier:startpath.stageexit}, ease:Power1.easeIn}));

      var carfinish32 = new TimelineMax()
         .add(TweenMax.to($("#carfinish_6"), 1, {css:{bezier:startpath.stageexit}, ease:Power1.easeIn}));

      var carfinish42 = new TimelineMax()
			.add(TweenMax.to($("#carfinish_7"), 1, {css:{bezier:startpath.stageexit}, ease:Power1.easeIn}));

      var carfinish52 = new TimelineMax()
         .add(TweenMax.to($("#carfinish_8"), 1, {css:{bezier:startpath.stageexit}, ease:Power1.easeIn}));



      //------------ build left to right scenes
		var scene = new ScrollMagic.Scene({triggerElement: '#target', duration: 1800, offset: 100})
						.setPin("#target")
						.setTween(car1)
						.addTo(controller);

      var scene2 = new ScrollMagic.Scene({triggerElement: '#target', duration: 1800, offset: 100})
						.setPin("#target")
						.setTween(car2)
						.addTo(controller);

      var scene3 = new ScrollMagic.Scene({triggerElement: '#target', duration: 1800, offset: 100})
						.setPin("#target")
						.setTween(car3)
						.addTo(controller);


      //------------ build right to left scenes
		var rtlscene1 = new ScrollMagic.Scene({triggerElement: '#rtlCarTarget', duration: 1000, offset: 100})
						.setPin("#rtlCarTarget")
						.setTween(carrtl1)
						.addTo(controller);

      var rtlscene2 = new ScrollMagic.Scene({triggerElement: '#rtlCarTarget', duration: 850, offset: 100})
						.setPin("#rtlCarTarget")
						.setTween(carrtl2)
						.addTo(controller);

      var rtlscene3 = new ScrollMagic.Scene({triggerElement: '#rtlCarTarget', duration: 800, offset: 100})
						.setPin("#rtlCarTarget")
						.setTween(carrtl3)
						.addTo(controller);


     //------- aim

     var aim_1 = new ScrollMagic.Scene({triggerElement: '#aim_first', duration: 600, offset: 100})
                 .addTo(controller);

     var aim_2 = new ScrollMagic.Scene({triggerElement: '#aim_second', duration: 600, offset: 100})
                 .addTo(controller);

     var aim_3 = new ScrollMagic.Scene({triggerElement: '#aim_third', duration: 600, offset: 100})
                .addTo(controller);

       aim_1.on("enter", function (event) {

          ga('send', 'event', 'scrollTo', 'view-mpu', 'first pre-order MPU');

       });

       aim_2.on("enter", function (event) {

         ga('send', 'event', 'scrollTo', 'view-mpu', 'second pre-order MPU');

       });

       aim_3.on("enter", function (event) {

          ga('send', 'event', 'scrollTo', 'view-mpu', 'third pre-order MPU');

       });






      var headerstart4 = new ScrollMagic.Scene({triggerElement: '#target2', duration: 800, offset: -180})
						.setTween(car12)
						.addTo(controller);

      var headerstart5 = new ScrollMagic.Scene({triggerElement: '#target2', duration: 700, offset: -180})
						.setTween(car22)
						.addTo(controller);

      var headerstart6 = new ScrollMagic.Scene({triggerElement: '#target2', duration: 600, offset: -180})
						.setTween(car32)
						.addTo(controller);

      var headerstart7 = new ScrollMagic.Scene({triggerElement: '#target2', duration: 700, offset: -180})
   					.setTween(car42)
   					.addTo(controller);

      var headerstart8 = new ScrollMagic.Scene({triggerElement: '#target2', duration: 800, offset: -180})
   					.setTween(car52)
   					.addTo(controller);



      var footerfinish4 = new ScrollMagic.Scene({triggerElement: '#finish', duration: 1100, offset: -580})
						.setTween(carfinish12)
						.addTo(controller);

      var footerfinish5 = new ScrollMagic.Scene({triggerElement: '#finish', duration: 900, offset: -580})
						.setTween(carfinish22)
						.addTo(controller);

      var footerfinish6 = new ScrollMagic.Scene({triggerElement: '#finish', duration: 1000, offset: -580})
						.setTween(carfinish32)
						.addTo(controller);

      var footerfinish7 = new ScrollMagic.Scene({triggerElement: '#finish', duration: 800, offset: -580})
   					.setTween(carfinish42)
   					.addTo(controller);

      var footerfinish8 = new ScrollMagic.Scene({triggerElement: '#finish', duration: 1000, offset: -580})
   					.setTween(carfinish52)
   					.addTo(controller);




      //-------stadium parallaxing------//
      var stadiummove = {
			moveup : {
				curviness: 0,
				autoRotate: false,
				values: [
						{x: 0,	y: 0},
						{x: 0,	y: -260}
					]
			}
		};

      var lightburstmove = {
			moveup : {
				curviness: 0,
				autoRotate: false,
				values: [
						{x: 0,	y: 0},
						{x: 0,	y: -300}
					]
			}
		};

      var stadiumpath = new TimelineMax()
			.add(TweenMax.to($("#stadium"), 1, {css:{bezier:stadiummove.moveup}, ease:Power1.easeIn}));


      var stadiumscene = new ScrollMagic.Scene({triggerElement: '#finish', duration: 300, offset: 200})
                  .setTween(stadiumpath)
                  .addTo(controller);

      var lightburstpath = new TimelineMax()
   		.add(TweenMax.to($("#lightburst"), 1, {css:{bezier:lightburstmove.moveup}, ease:Power1.easeIn}));


      var lightburstscene = new ScrollMagic.Scene({triggerElement: '#finish', duration: 450, offset: 200})
                  .setTween(lightburstpath)
                  .addTo(controller);

      var podiumpath = new TimelineMax()
			.add(TweenMax.to($("#podium"), 1, {css:{bezier:stadiummove.moveup}, ease:Power1.easeIn}));


      var podiumscene = new ScrollMagic.Scene({triggerElement: '#finish', duration: 300, offset: 200})
                  .setTween(podiumpath)
                  .addTo(controller);

      var bgcarspath = new TimelineMax()
			.add(TweenMax.to($("#bgcars"), 1, {css:{bezier:stadiummove.moveup}, ease:Power1.easeIn}));


      var bgcarsscene = new ScrollMagic.Scene({triggerElement: '#finish', duration: 400, offset: 200})
                  .setTween(bgcarspath)
                  .addTo(controller);

      var carfirstpath = new TimelineMax()
			.add(TweenMax.to($("#carfirst"), 1, {css:{bezier:stadiummove.moveup}, ease:Power1.easeIn}));


      var carfirstscene = new ScrollMagic.Scene({triggerElement: '#finish', duration: 450, offset: 200})
                  .setTween(carfirstpath)
                  .addTo(controller);


      var finishtriggerscene = new ScrollMagic.Scene({triggerElement: '#jsFinishTrigger', duration: 1000, offset: 0})
                  .addTo(controller);


      //-------end stadium parallaxing------//




      //---------   pap flashes on the intro     -------//

      function papFlash(paps){

         paps.find('.flash').each(function(i){

            var randTop = Math.floor((Math.random() * 200) + 1),
               randLeft = Math.floor((Math.random() * 350) + 1),
               randH = 90 + Math.floor((Math.random() * 30) + 1);

            $(this).css('top', randTop).css('left', randLeft).css('height', randH).css('width', randH);

         });

         paps.find('.flashwrapper').each(function(i){

            (function($set){
                 setInterval(function(){
                      var $cur = $set.find('.current').removeClass('current');
                      var $next = $cur.next().length?$cur.next():$set.children().eq(0);
                      $next.addClass('current');
                 },30);
             })($(this));


         });

      }

      // trigger flashes to stop them multiplying
      papFlash($('#paps'));
      papFlash($('#paps2'));

      scene2.on("enter", function (event) {

         console.log('>>>>>>>enter top');

         $('#paps').show();


      });

      scene2.on("leave", function (event) {

         console.log('>>>>>>>leave top');

         $('#paps').hide();

      });

      finishtriggerscene.on("enter", function (event) {

         $('#paps2').show();

         if($('.fireworks').length == 0) {

            $('#finish').find('.titletype__wrapper').prepend('<div class="fireworks left fireworks__wrapper"><img src="img/fireworks/13.gif" alt=""><img src="img/fireworks/14.gif" alt=""></div>');
            $('#finish').find('.titletype__wrapper').prepend('<div class="fireworks right fireworks__wrapper"><img src="img/fireworks/14.gif" alt=""><img src="img/fireworks/13.gif" alt=""></div>');

         }

      });

      finishtriggerscene.on("leave", function (event) {

         $('#paps2').hide();

         //$('.fireworks').remove();

      });


      //---------   END pap flashes on the intro     -------//




   //
	// });
   //
   //
   // $(function(){


      	// build scenes
      	var parallaxscene1 = new ScrollMagic.Scene({triggerElement: '#parallax1'})
      					.setTween("#parallax1 > div", {y: "70%", ease: Linear.easeNone})
      					.addTo(parallaxcontroller);

         var parallaxscene2 = new ScrollMagic.Scene({triggerElement: '#parallax2'})
      					.setTween("#parallax2 > div", {y: "70%", ease: Linear.easeNone})
      					.addTo(parallaxcontroller);

         var parallaxscene3 = new ScrollMagic.Scene({triggerElement: '#parallax3'})
      					.setTween("#parallax3 > div", {y: "30%", ease: Linear.easeNone})
      					.addTo(parallaxcontroller);


         var parallaxscene4 = new ScrollMagic.Scene({triggerElement: '#parallax4'})
      					.setTween("#parallax4 > div", {y: "30%", ease: Linear.easeNone})
      					.addTo(parallaxcontroller);


         var parallaxscene5 = new ScrollMagic.Scene({triggerElement: '#parallax5'})
      					.setTween("#parallax5 > div", {y: "30%", ease: Linear.easeNone})
      					.addTo(parallaxcontroller);


         var parallaxscene6 = new ScrollMagic.Scene({triggerElement: '#parallax6'})
                     .setTween("#parallax6 > div", {y: "30%", ease: Linear.easeNone})
                     .addTo(parallaxcontroller);


         var parallaxscene7 = new ScrollMagic.Scene({triggerElement: '#parallax7'})
                     .setTween("#parallax7 > div", {y: "30%", ease: Linear.easeNone})
                     .addTo(parallaxcontroller);


         var parallaxscene8 = new ScrollMagic.Scene({triggerElement: '#parallax8'})
                     .setTween("#parallax8 > div", {y: "30%", ease: Linear.easeNone})
                     .addTo(parallaxcontroller);


         var parallaxscene9 = new ScrollMagic.Scene({triggerElement: '#parallax9'})
                     .setTween("#parallax9 > div", {y: "30%", ease: Linear.easeNone})
                     .addTo(parallaxcontroller);

         var parallaxscene10 = new ScrollMagic.Scene({triggerElement: '#parallax10'})
                     .setTween("#parallax10 > div", {y: "30%", ease: Linear.easeNone})
                     .addTo(parallaxcontroller);


         var parallaxscene11 = new ScrollMagic.Scene({triggerElement: '#parallax11'})
                     .setTween("#parallax11 > div", {y: "30%", ease: Linear.easeNone})
                     .addTo(parallaxcontroller);




         //---------   date parallaxing   -------//


         var datetween1 = new TimelineMax ()
   			.add(TweenMax.to("#lrgDate1", 1, {top: 150, left: 0, ease: Linear.easeNone}));
         var datetween2 = new TimelineMax ()
   			.add(TweenMax.to("#lrgDate2", 1, {top: 150, left: 0, ease: Linear.easeNone}));
         var datetween3 = new TimelineMax ()
   			.add(TweenMax.to("#lrgDate3", 1, {top: 150, left: 0, ease: Linear.easeNone}));
         var datetween4 = new TimelineMax ()
   			.add(TweenMax.to("#lrgDate4", 1, {top: 150, left: 0, ease: Linear.easeNone}));
         var datetween5 = new TimelineMax ()
            .add(TweenMax.to("#lrgDate5", 1, {top: 150, left: 0, ease: Linear.easeNone}));
         var datetween6 = new TimelineMax ()
            .add(TweenMax.to("#lrgDate6", 1, {top: 150, left: 0, ease: Linear.easeNone}));
         var datetween7 = new TimelineMax ()
            .add(TweenMax.to("#lrgDate7", 1, {top: 150, left: 0, ease: Linear.easeNone}));
         var datetween8 = new TimelineMax ()
            .add(TweenMax.to("#lrgDate8", 1, {top: 150, left: 0, ease: Linear.easeNone}));
         var datetween9 = new TimelineMax ()
            .add(TweenMax.to("#lrgDate9", 1, {top: 150, left: 0, ease: Linear.easeNone}));


   		// build scene
   		var parallaxDate1 = new ScrollMagic.Scene({triggerElement: "#lrgDateTrigger1", duration: 1500, offset: 400})
   						.setTween(datetween1)
   						.addTo(parallaxcontroller);

         var parallaxDate2 = new ScrollMagic.Scene({triggerElement: "#lrgDateTrigger2", duration: 1500, offset: 400})
   						.setTween(datetween2)
   						.addTo(parallaxcontroller);

         var parallaxDate3 = new ScrollMagic.Scene({triggerElement: "#lrgDateTrigger3", duration: 1500, offset: 400})
   						.setTween(datetween3)
   						.addTo(parallaxcontroller);

         var parallaxDate4 = new ScrollMagic.Scene({triggerElement: "#lrgDateTrigger4", duration: 1500, offset: 400})
   						.setTween(datetween4)
   						.addTo(parallaxcontroller);

         var parallaxDate5 = new ScrollMagic.Scene({triggerElement: "#lrgDateTrigger5", duration: 1500, offset: 400})
   						.setTween(datetween5)
   						.addTo(parallaxcontroller);

         var parallaxDate6 = new ScrollMagic.Scene({triggerElement: "#lrgDateTrigger6", duration: 1500, offset: 400})
   						.setTween(datetween6)
   						.addTo(parallaxcontroller);

         var parallaxDate7 = new ScrollMagic.Scene({triggerElement: "#lrgDateTrigger7", duration: 1500, offset: 400})
   						.setTween(datetween7)
   						.addTo(parallaxcontroller);

         var parallaxDate8 = new ScrollMagic.Scene({triggerElement: "#lrgDateTrigger8", duration: 1500, offset: 400})
   						.setTween(datetween8)
   						.addTo(parallaxcontroller);

         var parallaxDate9 = new ScrollMagic.Scene({triggerElement: "#lrgDateTrigger9", duration: 1500, offset: 400})
   						.setTween(datetween9)
   						.addTo(parallaxcontroller);

         //---------   END date parallaxing   -------//




         //------- START text start ----//

         var starttexttween = new TimelineMax ()
            .add(TweenMax.to("#starttext", 1, {left: 1400, ease: Circ.easeInOut}));


         var starttext = new ScrollMagic.Scene({triggerElement: "#starttextTrigger", duration: 2000, offset: -200})
                     .setTween(starttexttween)
                     .addTo(parallaxcontroller);

         //----- END start text ------//


         //------- FINISH text start ----//

         var finishtexttween = new TimelineMax ()
            .add(TweenMax.to("#finishtext", 1, {left: 1400, ease: Circ.easeInOut}));


         var finishtext = new ScrollMagic.Scene({triggerElement: "#finishtextTrigger", duration: 2000, offset: -200})
                     .setTween(finishtexttween)
                     .addTo(parallaxcontroller);

         //----- END FINISH start text ------//



         //------- START console ----//

         var consoletween = new TimelineMax ()
            .add(TweenMax.to("#console1", 1, {top: 200, ease: Linear.easeNone}));


         var console1 = new ScrollMagic.Scene({triggerElement: "#consoleTrigger1", duration: 1400, offset: -200})
                     .setTween(consoletween)
                     .addTo(parallaxcontroller);

         var consoletween2 = new TimelineMax ()
            .add(TweenMax.to("#console2", 1, {top: 200, ease: Linear.easeNone}));


         var console2 = new ScrollMagic.Scene({triggerElement: "#consoleTrigger2", duration: 1400, offset: -200})
                     .setTween(consoletween2)
                     .addTo(parallaxcontroller);


         var consoletween3 = new TimelineMax ()
            .add(TweenMax.to("#console3", 1, {top: 200, ease: Linear.easeNone}));


         var console3 = new ScrollMagic.Scene({triggerElement: "#consoleTrigger3", duration: 1400, offset: -200})
                     .setTween(consoletween3)
                     .addTo(parallaxcontroller);


         var consoletween4 = new TimelineMax ()
            .add(TweenMax.to("#console4", 1, {top: 200, ease: Linear.easeNone}));


         var console4 = new ScrollMagic.Scene({triggerElement: "#consoleTrigger4", duration: 1400, offset: -200})
                     .setTween(consoletween4)
                     .addTo(parallaxcontroller);


         var consoletween5 = new TimelineMax ()
            .add(TweenMax.to("#console5", 1, {top: 200, ease: Linear.easeNone}));


         var console5 = new ScrollMagic.Scene({triggerElement: "#consoleTrigger5", duration: 1400, offset: -200})
                     .setTween(consoletween5)
                     .addTo(parallaxcontroller);


         var consoletween6 = new TimelineMax ()
            .add(TweenMax.to("#console6", 1, {top: 200, ease: Linear.easeNone}));


         var console6 = new ScrollMagic.Scene({triggerElement: "#consoleTrigger6", duration: 1400, offset: -200})
                     .setTween(consoletween6)
                     .addTo(parallaxcontroller);


         var consoletween7 = new TimelineMax ()
            .add(TweenMax.to("#console7", 1, {top: 200, ease: Linear.easeNone}));


         var console7 = new ScrollMagic.Scene({triggerElement: "#consoleTrigger7", duration: 1400, offset: -200})
                     .setTween(consoletween7)
                     .addTo(parallaxcontroller);



         var consoletween8 = new TimelineMax ()
            .add(TweenMax.to("#console8", 1, {top: 200, ease: Linear.easeNone}));


         var console8 = new ScrollMagic.Scene({triggerElement: "#consoleTrigger8", duration: 1400, offset: -200})
                     .setTween(consoletween8)
                     .addTo(parallaxcontroller);


         var consoletween9 = new TimelineMax ()
            .add(TweenMax.to("#console9", 1, {top: 200, ease: Linear.easeNone}));


         var console9 = new ScrollMagic.Scene({triggerElement: "#consoleTrigger9", duration: 1400, offset: -200})
                     .setTween(consoletween9)
                     .addTo(parallaxcontroller);


         var consoletween10 = new TimelineMax ()
            .add(TweenMax.to("#console10", 1, {top: 200, ease: Linear.easeNone}));


         var console10 = new ScrollMagic.Scene({triggerElement: "#consoleTrigger10", duration: 1400, offset: -200})
                     .setTween(consoletween10)
                     .addTo(parallaxcontroller);


         var consoletween11 = new TimelineMax ()
            .add(TweenMax.to("#console11", 1, {top: 200, ease: Linear.easeNone}));


         var console11 = new ScrollMagic.Scene({triggerElement: "#consoleTrigger11", duration: 1400, offset: -200})
                     .setTween(consoletween11)
                     .addTo(parallaxcontroller);

         //----- END console ------//




      var sidewaystween1 = new TimelineMax ()
         .add(TweenMax.to("#carsidewaysrtl1", 1, {right: 1400, ease: Circ.easeInOut}));

      var sidewaystween2 = new TimelineMax ()
         .add(TweenMax.to("#carsidewaysrtl2", 1, {right: 1700, ease: Circ.easeInOut}));

      var sidewaystween3 = new TimelineMax ()
         .add(TweenMax.to("#carsidewaysrtl3", 1, {right: 2200, ease: Circ.easeInOut}));

      var sidewaystween4 = new TimelineMax ()
         .add(TweenMax.to("#carsidewaysltr4", 1, {left: 1700, ease: Circ.easeInOut}));

      var sidewaystween5 = new TimelineMax ()
         .add(TweenMax.to("#carsidewaysltr5", 1, {left: 2100, ease: Circ.easeInOut}));

      var sidewaystween6 = new TimelineMax ()
         .add(TweenMax.to("#carsidewaysltr6", 1, {left: 2500, ease: Circ.easeInOut}));

      var smokeTween = new TimelineMax ()
         .add(TweenMax.to("#smoke", 1, {opacity: 1, ease: Circ.easeInOut}));

      var smokeTween2 = new TimelineMax ()
         .add(TweenMax.to("#smoke2", 1, {opacity: 1, ease: Circ.easeInOut}));


      //drift tweens
      var drifttween1 = new TimelineMax ()
         .add(TweenMax.to("#leftskid1", 0.5, {left: 200, ease: Circ.easeOut}));

      var drifttween2 = new TimelineMax ()
         .add(TweenMax.to("#leftskid2", 0.7, {left: 750, ease: Circ.easeOut}));

      var drifttween3 = new TimelineMax ()
         .add(TweenMax.to("#leftskid3", 1, {left: 350, ease: Circ.easeOut}));



      //drift tweens
      var explosiontween1 = new TimelineMax ()
         .add(TweenMax.to("#explosioncarleft", 1, {left: 850, ease: Circ.easeIn}));

      var explosiontween2 = new TimelineMax ()
         .add(TweenMax.to("#explosioncarright", 1, {left: 950, ease: Circ.easeIn}));


      //wiggletween
      var wiggletween1 = new TimelineMax ()
         .add(TweenMax.to("#widdlecarleft", 1, {left: 250, ease: Circ.easeOut}));

      var wiggletween2 = new TimelineMax ()
         .add(TweenMax.to("#wigglecarright", 1, {right: 0, ease: Circ.easeOut}));

         var wigglemove = {
   			move : {
   				curviness: 1,
   				autoRotate: false,
   				values: [
   						{x: 100,	y: 0},
   						{x: 0,	y: 0},
                     {x: 100,	y: 0},
                     {x: 0,	y: 0}
   					]
   			}
   		};

         var wiggletween3 = new TimelineMax()
   			.add(TweenMax.to($("#wigglecarfront"), 1, {css:{bezier:wigglemove.move}, ease:Power1.easeInOut}));



       //------------ build right to left sideways scenes
       var rtlsidewaysscene1 = new ScrollMagic.Scene({triggerElement: '#sidewaysrtltarget', duration: 2000, offset: -300})
                .setTween(sidewaystween1)
                .addTo(parallaxcontroller);

        var rtlsidewaysscene2 = new ScrollMagic.Scene({triggerElement: '#sidewaysrtltarget', duration: 2000, offset: -300})
                 .setTween(sidewaystween2)
                 .addTo(parallaxcontroller);

        var rtlsidewaysscene3 = new ScrollMagic.Scene({triggerElement: '#sidewaysrtltarget', duration: 2000, offset: -300})
                 .setTween(sidewaystween3)
                 .addTo(parallaxcontroller);

        var smokescene = new ScrollMagic.Scene({triggerElement: '#sidewaysrtltarget', duration: 300, offset: -200})
                 .setTween(smokeTween)
                 .addTo(parallaxcontroller);


        var rtlsidewaysscene3 = new ScrollMagic.Scene({triggerElement: '#sidewaysltrtarget', duration: 2000, offset: -300})
                 .setTween(sidewaystween4)
                 .addTo(parallaxcontroller);

         var rtlsidewaysscene4 = new ScrollMagic.Scene({triggerElement: '#sidewaysltrtarget', duration: 2000, offset: -300})
                  .setTween(sidewaystween5)
                  .addTo(parallaxcontroller);

         var rtlsidewaysscene5 = new ScrollMagic.Scene({triggerElement: '#sidewaysltrtarget', duration: 2000, offset: -300})
                  .setTween(sidewaystween6)
                  .addTo(parallaxcontroller);

         var smokescene2 = new ScrollMagic.Scene({triggerElement: '#sidewaysltrtarget', duration: 300, offset: -200})
                  .setTween(smokeTween2)
                  .addTo(parallaxcontroller);



         //drift scenes
         var drift1 = new ScrollMagic.Scene({triggerElement: '#leftskid', duration: 500, offset: -300})
                  .setTween(drifttween1)
                  .addTo(parallaxcontroller);

          var drift2 = new ScrollMagic.Scene({triggerElement: '#leftskid', duration: 500, offset: -300})
                   .setTween(drifttween2)
                   .addTo(parallaxcontroller);

          var drift3 = new ScrollMagic.Scene({triggerElement: '#leftskid', duration: 500, offset: -300})
                   .setTween(drifttween3)
                   .addTo(parallaxcontroller);


          //explosion scenes
         var explosion1 = new ScrollMagic.Scene({triggerElement: '#explosionwrapper', duration: 1000, offset: -1100})
                  .setTween(explosiontween1)
                  .addTo(parallaxcontroller);

          var explosion2 = new ScrollMagic.Scene({triggerElement: '#explosionwrapper', duration: 1000, offset: -1100})
                   .setTween(explosiontween2)
                   .addTo(parallaxcontroller);


      //moving cars
       var wiggle1 = new ScrollMagic.Scene({triggerElement: '#wigglewrapper', duration: 2000, offset: -200})
                .setTween(wiggletween1)
                .addTo(parallaxcontroller);

        var wiggle2 = new ScrollMagic.Scene({triggerElement: '#wigglewrapper', duration: 2000, offset: -200})
                 .setTween(wiggletween2)
                 .addTo(parallaxcontroller);

        var wiggle3 = new ScrollMagic.Scene({triggerElement: '#wigglewrapper', duration: 3000, offset: -200})
                 .setTween(wiggletween3)
                 .addTo(parallaxcontroller);

         //  var explosion3 = new ScrollMagic.Scene({triggerElement: '#leftskid', duration: 500, offset: -300})
         //           .setTween(drifttween3)
         //           .addTo(parallaxcontroller);

         d = new Date();
         $("#explosion").find('img').attr("src", "img/explosiongif/ezgif.com-resize.gif?"+d.getTime()+1);

         explosion2.on("leave", function (event) {

            console.log(event.scrollDirection);

            // if(event.scrollDirection == 'REVERSE') {
            //
            //    console.log('>>>>>> leaving reverse');
            //    $('#explosioncarleft').css('opacity', 1);
            //    $('#explosioncarright').css('opacity', 1);
            //    $('#explosionpng').css('opacity', 1);
            // }

            $('#explosionpng').velocity({
               opacity : 1
            }, 100, function(){
               $('#explosion').show();

               $('#explosioncarleft').css('opacity', 0);
               $('#explosioncarright').css('opacity', 0);
               $('#explosionpng').css('opacity', 0);

            });

         });

         explosion2.on("enter", function (event) {

            console.log(event);

            if(event.scrollDirection !== 'REVERSE') {
                  $("#explosion").find('img').attr("src", "img/explosiongif/ezgif.com-resize.gif?"+d.getTime()+1);
                  $('#explosion').hide();

                  $('#explosioncarleft').css('opacity', 1);
                  $('#explosioncarright').css('opacity', 1);
                  //$('#explosionpng').css('opacity', 1);

            }

         });


         //---- START skid marks ----//

         // var skidmark1 = new ScrollMagic.Scene({triggerElement: "#skidTrigger1", duration: 1000, offset: 500})
         //             .addTo(parallaxcontroller);
         //
         // skidmark1.on("enter", function (event) {
         //
         //    $('#skid1').velocity({
         //       opacity : 0.2
         //    }, 600);
         //
         // });
         //
         //
         // var skidmark2 = new ScrollMagic.Scene({triggerElement: "#skidTrigger2", duration: 1000, offset: 500})
         //             .addTo(parallaxcontroller);
         //
         // skidmark2.on("enter", function (event) {
         //
         //    $('#skid2').velocity({
         //       opacity : 0.2
         //    }, 600);
         //
         // });



         //---- END skid marks ----//




         //--------  START featured panel scrolling -----//

         var featuredVideo1 = new TimelineMax ()
            .add(TweenMax.to("#videoGameFeatured1", 1, {top: 500, ease: Linear.easeNone}));
         var featuredVideo2 = new TimelineMax ()
            .add(TweenMax.to("#videoGameFeatured2", 1, {top: 2050, ease: Linear.easeNone}));
         var featuredVideo3 = new TimelineMax ()
            .add(TweenMax.to("#videoGameFeatured3", 1, {top: 3400, ease: Linear.easeNone}));
         var featuredVideo4 = new TimelineMax ()
            .add(TweenMax.to("#videoGameFeatured4", 1, {top: 7250, ease: Linear.easeNone}));
         var featuredVideo5 = new TimelineMax ()
            .add(TweenMax.to("#videoGameFeatured5", 1, {top: 200, ease: Linear.easeNone}));
         // var featuredVideo6 = new TimelineMax ()
         //    .add(TweenMax.to("#videoGameFeatured6", 1, {top: 10000, ease: Linear.easeNone}));
         // var featuredVideo7 = new TimelineMax ()
         //    .add(TweenMax.to("#videoGameFeatured7", 1, {top: 12000, ease: Linear.easeNone}));
         // var featuredVideo8 = new TimelineMax ()
         //    .add(TweenMax.to("#videoGameFeatured8", 1, {top: 14000, ease: Linear.easeNone}));


         var parallaxVid1 = new ScrollMagic.Scene({triggerElement: "#FeatureGameTrigger1", duration: 1500, offset: -400})
                     .setTween(featuredVideo1)
                     .addTo(parallaxcontroller);

         var parallaxVid2 = new ScrollMagic.Scene({triggerElement: "#FeatureGameTrigger2", duration: 1500, offset: -400})
                     .setTween(featuredVideo2)
                     .addTo(parallaxcontroller);

         var parallaxVid3 = new ScrollMagic.Scene({triggerElement: "#FeatureGameTrigger3", duration: 1500, offset: -400})
                     .setTween(featuredVideo3)
                     .addTo(parallaxcontroller);

         var parallaxVid4 = new ScrollMagic.Scene({triggerElement: "#FeatureGameTrigger4", duration: 1500, offset: -400})
                     .setTween(featuredVideo4)
                     .addTo(parallaxcontroller);

         var parallaxVid5 = new ScrollMagic.Scene({triggerElement: "#FeatureGameTrigger5", duration: 1500, offset: -400})
                     .setTween(featuredVideo5)
                     .addTo(parallaxcontroller);
         //
         // var parallaxVid6 = new ScrollMagic.Scene({triggerElement: "#FeatureGameTrigger6", duration: 1500, offset: -400})
         //             .setTween(featuredVideo6)
         //             .addTo(parallaxcontroller);
         //
         // var parallaxVid7 = new ScrollMagic.Scene({triggerElement: "#FeatureGameTrigger7", duration: 1500, offset: -400})
         //             .setTween(featuredVideo7)
         //             .addTo(parallaxcontroller);
         //
         // var parallaxVid8 = new ScrollMagic.Scene({triggerElement: "#FeatureGameTrigger8", duration: 1500, offset: -400})
         //             .setTween(featuredVideo8)
         //             .addTo(parallaxcontroller);


         //-------   END featured panel scrolling ----//






         var footertrigger = new ScrollMagic.Scene({triggerElement: '#footerTrigger', duration: 500, offset: 0})
                     .addTo(parallaxcontroller);


         footertrigger.on("enter", function (event) {

            // $('#fireworks').find('img').each(function(){
            //
            //    var $src = $(this).attr('src');
            //    $(this).remove();
            //    $('#fireworks').append('<img src="'+ $src +'" alt="">');
            //
            // });
            //
            //
            //$('#fireworks').show();

         });

         parallaxscene2.on("leave", function (event) {

            //$('#fireworks').hide();

         });




   });


$(window).bind("load", function() {

   if (!!navigator.userAgent.match(/Version\/[\d\.]+.*Safari/)) {
       $('body').addClass('safari');
   }

   function isIE(userAgent) {
     userAgent = userAgent || navigator.userAgent;
     return userAgent.indexOf("MSIE ") > -1 || userAgent.indexOf("Trident/") > -1;
   }

   //ie alert
   if (isIE() == true){ //test for MSIE x.x;


            $('#dragmodal').velocity({
             opacity : 1
            }, 1000, 'easeOutSine', function(){
               $(this).css('z-index', '99999');
            });

            $('#read').on('click', function(){

               $('#dragmodal').velocity({
                opacity : 0
               }, 1000, 'easeOutSine', function(){});

            });


   } else {





   }

});
