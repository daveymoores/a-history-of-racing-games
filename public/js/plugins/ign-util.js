(function(window, document, $) {
  var IGN = window.IGN = window.IGN || {};

  // initialize _gaq array so any GA events will be queued up if the ga.js library loads slowly
  window._gaq = (typeof window._gaq === 'undefined') ? [] : window._gaq;

  IGN.jQuery = jQuery;
  IGN.jQueryVersions = {};

  IGN.registerJQuery = function(jQuery) {
    var version = jQuery.fn.jquery.replace(/\./g, '_');
    IGN.jQueryVersions[version] = jQuery;
  };

  IGN.registerJQuery(jQuery);

  /*
   * Cookie functions
   * expires - int that represents days
   */
  IGN.createCookie = function(name, content, expires, domain) {
    if (expires) {
      var date = new Date();
      date.setTime(date.getTime()+(expires*24*60*60*1000));
      var expires = '; expires='+date.toGMTString();
    } else {
      var expires = '';
    }

    domain  = typeof domain !== 'undefined' ? domain : '.ign.com';

    document.cookie = name+'='+content+expires+';domain='+domain+';path=/';
  };

  // returns cooke with matching name
  IGN.readCookie = function(name) {
    var cookies = document.cookie.split('; ');
    for (var i=0; i < cookies.length; i++) {
      if (cookies[i].split('=')[0] === name) {
        return cookies[i];
      }
    }
    return null;
  };

  IGN.readCookieValue = function(name) {
    var cookie = this.readCookie(name);

    if(cookie) {
      cookie = cookie.split('=');
      return cookie[1];
    }
  }

  IGN.eraseCookie = function(name, domain) {
    domain  = typeof domain !== 'undefined' ? domain : '.ign.com';
    this.createCookie(name, "", -1, domain);
  };

  /* clones javascript object instead of just referencing */
  IGN.clone = function(obj) {
      if (null == obj || "object" != typeof obj) return obj;
      var copy = obj.constructor();
      for (var attr in obj) {
          if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
      }
      return copy;
  };

  /* Math functions used for SVG manipulation */
  IGN.polarToCartesian = function(cX, cY, r, aD) {
    var aR = (aD-90) * Math.PI / 180.0;

    return {
      x: cX + (r * Math.cos(aR)),
      y: cY + (r * Math.sin(aR))
    };
  };

  IGN.describeArc = function(x, y, radius, startAngle, endAngle) {
    var start = this.polarToCartesian(x, y, radius, endAngle),
      end = this.polarToCartesian(x, y, radius, startAngle),
      arcSweep = endAngle - startAngle <= 180 ? "0" : "1",
      d = [
        "M", start.x, start.y,
        "A", radius, radius, 0, arcSweep, 0, end.x, end.y
      ].join(" ");

    return d;
  };

  IGN.isMobilePlatform = function() {
    var iosDevice = navigator.userAgent.match(/(iPod|iPhone|iPad)/);
    var androidDevice = navigator.userAgent.toLowerCase().match('android');
    return (androidDevice != null || iosDevice != null);
  };

  /*
   * Centers elements whose width can vary.
   * Usage: simply add 'util-center' class to element and make sure it has a parent to
   * center on. Use overflow: hidden on parent to center crop.
   */
  IGN.centerElementsWithVariableWidth = function() {
    $('.util-center').each(function() {
      $(this).css('position', 'relative')
             .css('left', '50%')
             .css('margin-left', '-'+Math.floor($(this).width()/2)+'px');
    });
  };

  /*
   * Gets the widget domain off of the body data attr.
   */
  IGN.getWidgetDomain = function () {
    if (this.widgetDomain) {
      return this.widgetDomain;
    }

    return this.widgetDomain = $('body').data('widgetDomain') || 'widgets.ign.com';
  };

  /*
   * Loads an IGN video player through ajax into a given dom element.
   */
  IGN.ajaxLoadVideo = function (overrides, $el, callback) {
    if (typeof overrides === 'undefined' || typeof $el === 'undefined') return;

    var data = {
      autoplay : true,
      companions : false,
      width : '100%',
      height : '100%'
    };

    $.extend(data, overrides);

    $.ajax({
      url: 'http://' + IGN.getWidgetDomain() + '/video/embed/content.jsonp',
      data: data,
      dataType: 'jsonp',
      success: function (data) {
        var $vjs = $el.find('.video-js');

        if (typeof window.videojs !== 'undefined' && $vjs.length) {
          $vjs.each(function(){
            window.videojs($(this)[0]).dispose();
          });
        }

        $el.html(data);

        if(typeof callback === 'function') {
          callback.call();
        }
      }
    });
  };

  /*
   * Reloads all comment counts on a page.
   */
  IGN.reloadCommentCounts = function() {
    window.DISQUSWIDGETS = undefined;
    $.getScript("http://" + disqus_shortname + ".disqus.com/count.js");
  }

  /*
   * For events that fire multiple times before calcs need to be made (such as window resize).
   * Accepts callback function and ms threshold as to when to fire the callback.
   * uniqueId is so that we can keep track of multiple timers
   * Adopted from http://stackoverflow.com/questions/2854407/javascript-jquery-window-resize-how-to-fire-after-the-resize-is-completed#answer-4541963
   */
  IGN.waitForFinalEvent = (function () {
    IGN.timers = IGN.timers || {};
    return function (callback, ms, uniqueId) {
      if (!uniqueId) {
        console.error('Missing param uniqueId in IGN.waitForFinalEvent()');
      }
      if (IGN.timers[uniqueId]) {
        clearTimeout(IGN.timers[uniqueId]);
      }
      IGN.timers[uniqueId] = setTimeout(callback, ms);
    };
  })();

  /*
   * Uses current domain to retrieve URL for oystatic files
   * i.e.: http://local.dev.oystatic.ignimgs.com:10088/core/js/external/neon/neon-ign-main.js
   */
  IGN.getOystaticUrl = function () {
    var protocol = window.location.protocol,
      currentHostname = window.location.hostname,
      oystaticHostname;

    if (currentHostname.indexOf('local') !== -1) {
      oystaticHostname = "local.dev.oystatic.ignimgs.com";
    } else if (currentHostname.indexOf('stg') !== -1 ) {
      var domainParts = currentHostname.split('.');
      var stgName = domainParts[0];
      oystaticHostname = 'o.stg.oystatic.ignimgs.com/' + stgName + '/static';
    } else {
      oystaticHostname = "oystatic.ignimgs.com/src";
    }

    return protocol + "//" + oystaticHostname;
  };

  /*
   * Taken from http://stackoverflow.com/questions/7718935/load-scripts-asynchronously
   * this function will work cross-browser for loading scripts asynchronously
   */
  IGN.loadAsync = function (src, callback) {
    var s,
        r,
        t;
    r = false;
    s = document.createElement('script');
    s.type = 'text/javascript';
    s.src = src;
    s.onload = s.onreadystatechange = function() {
      //console.log( this.readyState ); //uncomment this line to see which ready states are called.
      if ( !r && (!this.readyState || this.readyState == 'complete') )
      {
        r = true;
        if (typeof callback !== 'undefined') {
          callback();
        }
      }
    };
    t = document.getElementsByTagName('script')[0];
    t.parentNode.insertBefore(s, t);
  };
})(window, document, jQuery);

/*
 * IGNScroll object
 *  - handles scroll events
 *  - syncs scroll event callback execution with requestAnimationFrame
 *  - interfaces with jQuery's event extensions
 *  - NOTE: you *have* to attach IGNScroll listeners to either $(window) or $('body'), no exceptions
 *
 *  - !TODO: observes registered elements and updates thresholds based on their mutations
 *
 * Use cases:
 *  - adding a "static" listener triggered when scrolling DOWN
 *      $(window).on('IGNScroll.staticUpExample', {
 *          threshold: 200
 *      }, callbackFunction);
 *
 *  - adding a "static" listener triggered when scrolling UP
 *      $(window).on('IGNScroll.staticDownExample', {
 *          threshold: 200,
 *          reverse: true
 *      }, callbackFunction);
 *
 *  - adding a "dynamic" listener triggered when scrolling UP
 *      $(window).on('IGNScroll.dynamicUpExample', {
 *          threshold: function () { return Number.parseInt(Math.random() * 10); }
 *      }, callbackFunction);
 *
 *  - removing a scroll listener
 *      $(window).off('IGNScroll.dynamicUpExample');
 *
 *  - force update a scroll listener (doesn't do anything for static thresholds)
 *      IGNScroll.update('dynamicUpExample');
 *
 *  - force update all scroll listeners (doesn't do anything for static thresholds)
 *      IGNScroll.update();
 *
 *
 * Legacy jQuery versions (< 1.7)
 *  - use $.delegate() instead of $.on(), and $.undelegate() instead of $.off()
 *      $('html').delegate('body', 'IGNScroll.delegateExample', {
 *        threshold: 100
 *      }, callbackFunction);
 *
 */
var IGNScroll = (function ($) {

  /*
   * Threshold object
   *  - stores thresholds (pageYoffset) that IGNScroll checks and uses for scroll event triggering
   *  - can be:
   *    1. "dynamic" if the threshold parameter is a function that returns a number, OR
   *    2. "static" if the threshold parameter is a number
   *  - "dynamic" thresholds can be updated/recalculated by calling Threshold.update()
   *  - "continous" thresholds trigger their events every frame
   */
  var Threshold = function (listener, threshold, reverse, continuous) {

    switch (true) {

      case (typeof threshold === 'number'):
        this.threshold   = threshold;
        this.thresholdFn = null;
        this.dynamic     = false;
      break;

      case (typeof threshold === 'function'):
        var temp = threshold();
        if (typeof temp === 'number') {
          this.threshold   = temp;
          this.thresholdFn = threshold;
          this.dynamic     = true;
          break;
        }

      default:
        throw {
          error: 'IGNScroll: Failure to initialize threshold. Threshold parameter must be a number or a function that returns a number.',
          parameters: arguments
        };
      break;
    }

    this.listener   = listener;
    this.reverse    = reverse    === true ? true : false;
    this.continuous = continuous === true ? true : false;
  }

  Threshold.prototype.update = function() {
    if (this.dynamic) this.threshold = this.thresholdFn();
  };

  return IGNScroll = {

    init: function ($) {
      // using _map for uniqueness checks and _thresholds for update loops
      this.$body       = $('body');
      this._map        = this._map || {};
      this._thresholds = this._thresholds || [];

      this._state = {
        enabled       : true,
        debug         : false,
        delayFrame    : false,
        lastScrollY   : 0,
        observedChange: false,
        observerInit  : false,
        scrollY       : window.pageYOffset
      };

      // jQuery event interface
      $.event.special.IGNScroll = {
        add: function( handler ) {
          if (typeof handler.namespace === 'string' && typeof handler.data === 'object') {
            IGNScroll.add(
              handler.namespace,
              handler.data.threshold,
              handler.data.reverse,
              handler.data.continuous
            );
          }
        },
        remove: function( handler ) {
          if (typeof handler.namespace === 'string') {
            IGNScroll.remove(
              handler.namespace
            );
          }
        }
      };

      // expanding leaderboard hack
      // $(window).on('topLeaderboardLoaded', function() {
      //     this.addObserver('.ign-pre-grid, .preShell');
      // }.bind(this));

      window.addEventListener('scroll', this._onScroll.bind(this), false);

      // "back" button support
      this._onScroll();

      if (this._state.debug) console.log('DEBUG: IGNScroll initialized');

      return this;
    },

    // rAF polyfill
    _requestAnimationFrame: (function () {
      return window.requestAnimationFrame
        || window.webkitRequestAnimationFrame
        || window.mozRequestAnimationFrame
        || window.oRequestAnimationFrame
        || window.msRequestAnimationFrame
        || function(callback) { return window.setTimeout(callback, 1000 / 60);
      }
    })(),

    // runs on every scroll event
    _onScroll: function (evt) {
      if (this._state.enabled && !this._state.delayFrame) {
        this._requestAnimationFrame.call(window, this._handleEvents.bind(this));
        this._state.delayFrame = true;
      }
    },

    // runs on every frame
    _handleEvents: function () {
      this._state.scrollY = window.pageYOffset; // fastest offset lookup, don't change

      if (this._state.scrollY !== this._state.lastScrollY || this._state.observedChange) {

        var index = this._thresholds.length;
        while (index-- > 0) {

          // trigger IGNScroll event for crossed thresholds
          if ((!this._thresholds[index].reverse && (this._thresholds[index].continuous || this._state.lastScrollY <= this._thresholds[index].threshold) && this._state.scrollY > this._thresholds[index].threshold)
            || (this._thresholds[index].reverse && (this._thresholds[index].continuous || this._state.lastScrollY >= this._thresholds[index].threshold) && this._state.scrollY < this._thresholds[index].threshold)) // I'm so sorry!
          {
            if (this._state.debug) console.log('IGNScroll::_handleEvents> triggering "' + 'IGNScroll.' + this._thresholds[index].listener + '" at ' + this._state.scrollY);
            this.$body.trigger('IGNScroll.' + this._thresholds[index].listener);
          }

        }

        this._state.observedChange = false;
      }

      this._state.lastScrollY = this._state.scrollY;
      this._state.delayFrame = false;
    },

    // dynamic threshold update logic
    _handleThresholdUpdates: function (listener) {
      this._state.scrollY = window.pageYOffset; // fastest offset lookup, don't change
      this._state.observedChange = true;

      // if listener is passed in, use the "map" to update the specific threshold
      if (typeof listener === 'string' && this._map.hasOwnProperty(listener) && this._map[listener].dynamic) {
        var oldState = this._map[listener].threshold > this._state.scrollY;
        this._map[listener].update();

        // if update results in a missed event, force-trigger it
        if (oldState !== this._map[listener].threshold > this._state.scrollY) {
          if (this._state.debug) console.log('IGNScroll::_handleThresholdUpdates> triggering "' + 'IGNScroll.' + listener + '" at ' + this._state.scrollY);
          this.$body.trigger('IGNScroll.' + listener);
        }
        return;
      }

      // otherwise, update every dynamic
      var index = this._thresholds.length;
      while (index-- > 0) {
        if (this._thresholds[index].dynamic)
          this._thresholds[index].update();
      }
    },

    // public> adds a new scroll threshold
    add: function (listener, threshold, reverse, continuous) {
      if (this._state.debug) console.log('IGNScroll::add>', listener);
      if (typeof listener === 'string' && !this._map.hasOwnProperty(listener)) {
        try {
          this._map[listener] = new Threshold(listener, threshold, reverse, continuous);
          this._thresholds.push(this._map[listener]);
        } catch (e) {
          if (this._state.debug) console.log(e);

          return false;
        };
      }

      return this;
    },

    // public> update dynamic thresholds "manually"
    update: function (listener) {
      if (this._state.debug) console.log('IGNScroll::update>', typeof listener === 'string' ? listener : 'ALL');
      try {
        this._handleThresholdUpdates(listener);
      } catch (e) {
        if (this._state.debug) console.log(e);

        return false;
      }

      return this;
    },

    // public> remove thresholds
    remove: function (listener) {
      if (this._state.debug) console.log('IGNScroll::remove>', listener);
      if (typeof listener === 'string' && this._map.hasOwnProperty(listener)) {
        try {
          this._thresholds.splice(
            this._thresholds.indexOf(this._map[listener]), 1
          );
          delete this._map[listener];
        } catch (e) {
          if (this._state.debug) console.log(e);

          return false;
        }
      }

      return this;
    }
  };

})(jQuery);

(function($) {
  $(document).ready(function() {

    // Check for svg compatibility
    var img = new Image();
    img.onload = function() {
      $("html").addClass("has-svg");
    };
    img.src = 'data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIGJhc2VQcm9maWxlPSJ0aW55IiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIg0KCSB4PSIwcHgiIHk9IjBweCIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDMyIDMyIiBvdmVyZmxvdz0idmlzaWJsZSIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+DQo8cGF0aCBmaWxsPSIjRkZGRkZGIiBkPSJNMCwwLjYzM0wyMSwxNkwwLDMxLjM2N1YwLjYzM3ogTTMyLDBIMHYzMmgzMlYweiIvPg0KPC9zdmc+';

    IGN.centerElementsWithVariableWidth();

    // IGNScroll
    IGNScroll.init($);
  });

})(jQuery);
